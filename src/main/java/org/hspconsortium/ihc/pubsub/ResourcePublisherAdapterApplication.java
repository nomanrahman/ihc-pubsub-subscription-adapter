package org.hspconsortium.ihc.pubsub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ResourcePublisherAdapterApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(ResourcePublisherAdapterApplication.class);
	}

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(ResourcePublisherAdapterApplication.class, args);

		System.out.println("Welcome to Spring Boot with context: " + ctx);
	}
}
